# switch panes using vim bindings or arrows
bind Up select-pane -U
bind k select-pane -U
bind Down select-pane -D
bind j select-pane -D
bind Left select-pane -L
bind h select-pane -L
bind Right select-pane -R
bind l select-pane -R

# saner split pane commands
bind | split-window -h
bind - split-window -v

# no delay for escape key press
set -sg escape-time 0

# reload config file
bind R source-file ~/.tmux.conf \; display "~/.tmux.conf reloaded"

# mouse stuff
setw -g mouse on

# theme
set -g status-style bg=default,fg=white
set -g window-status-current-style bg=black,fg=white,bold
set -g pane-border-style fg=white
set -g pane-active-border-style fg=colour39,bg=default
set -g mode-style bg=colour39,fg=white,bold
set -g status-interval 5

# Start window and pane indices at 1.
set -g base-index 1
set -g pane-base-index 0

# Length of tmux status line
set -g status-left-length 30
set -g status-right-length 150

set-option -g status "on"

# Default statusbar color
set-option -g status-style bg=colour237,fg=colour223 # bg=bg1, fg=fg1

# Default window title colors
set-window-option -g window-status-style bg=colour214,fg=colour237 # bg=yellow, fg=bg1

# Default window with an activity alert
set-window-option -g window-status-activity-style bg=colour237,fg=colour248 # bg=bg1, fg=fg3

# Active window title colors
set-window-option -g window-status-current-style bg=red,fg=colour237 # fg=bg1

# Set active pane border color
set-option -g pane-active-border-style fg=colour214

# Set inactive pane border color
set-option -g pane-border-style fg=colour239

# Message info
set-option -g message-style bg=colour239,fg=colour223 # bg=bg2, fg=fg1

# Writing commands inactive
set-option -g message-command-style bg=colour239,fg=colour223 # bg=fg3, fg=bg1

# Pane number display
set-option -g display-panes-active-colour colour1 #fg2
set-option -g display-panes-colour colour237 #bg1

# Clock
set-window-option -g clock-mode-colour colour109 #blue

# Bell
set-window-option -g window-status-bell-style bg=colour167,fg=colour235 # bg=red, fg=bg

set-option -g status-left "\
#[fg=colour7, bg=colour241]#{?client_prefix,#[bg=colour167],} #S \
#[fg=colour241, bg=colour237]#{?client_prefix,#[fg=colour167],}#{?window_zoomed_flag, 🔍,} \
#[fg=colour1]  "

set-option -g status-right "\
#[fg=colour214, bg=colour237] \
#[fg=colour246, bg=colour237]  %a %d %b %Y\
#[fg=colour109]  %H:%M \
#[fg=colour248, bg=colour239]"

set-window-option -g window-status-current-format "\
#[fg=colour237, bg=colour214]\
#[fg=colour239, bg=colour214] #I* \
#[fg=colour239, bg=colour214, bold] #W \
#[fg=colour214, bg=colour237]"

set-window-option -g window-status-format "\
#[fg=colour237,bg=colour239,noitalics]\
#[fg=colour223,bg=colour239] #I \
#[fg=colour223, bg=colour239] #W \
#[fg=colour239, bg=colour237]"

# Set the history limit so we get lots of scrollback.
setw -g history-limit 50000000
# automatically set window title
set-window-option -g automatic-rename on
set-option -g set-titles on

# syncrhonize panes
bind E setw synchronize-panes

# start numbering at 1
set -g base-index 1
setw -g pane-base-index 1

# use vim binding in scroll/copy mode
setw -g mode-keys vi

# set scrollback buffer size
set-option -g history-limit 50000

# tpm
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'

if "test ! -d ~/.tmux/plugins/tpm" \
   "run 'git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm && ~/.tmux/plugins/tpm/bin/install_plugins'"
# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
