source /etc/profile

source $HOME/.aliases
source $HOME/.antigenrc
source $HOME/.p10k.zsh

[[ $- != *i* ]] && return
[[ -z "$TMUX" ]] && exec tmux

