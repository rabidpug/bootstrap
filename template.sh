#!/bin/bash
set -eou pipefail

BS_PATH=/usr/local/bootstrap

# Clone bootstrap repo to /usr/local/bootstrap and set up
mkdir -p "$BS_PATH"
git clone -q https://gitlab.com/rabidpug/bootstrap.git "$BS_PATH"
touch "$BS_PATH/.env"

# Define required variables for scripts. ? have defaults or are optional
cat <<EOT >>$BS_PATH/.env
GITHUB_AUTH_TOKEN=
DO_AUTH_TOKEN=
LIVEPATCH_KEY=
USERNAME=?
PASSWORD=?
GIT_NAME=?
GIT_EMAIL=?
DOMAIN=?
ID_RSA=
ID_RSA_PUB=
REPOS=(
  ?
)
TZ=?
DEBUG=?
EOT

touch /var/log/bs.log
bash "$BS_PATH/bs" &>>/var/log/bs.log
