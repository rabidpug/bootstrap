#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

home_directory="$(eval echo ~$USERNAME)"
vsix_directory="$home_directory/.local/share/code-server/vsix"
ls "$vsix_directory" | sed 's/\(.*\)-[0-9]*.[0-9]*.[0-9]*.vsix/\1/' | while read ext; do
  if [ -z "$(code-server --list-extensions | grep $ext)" ]; then
    lg "$ext not installed - removing"
    rm "$vsix_directory/$(ls $vsix_directory | grep $ext)"
  fi
done
