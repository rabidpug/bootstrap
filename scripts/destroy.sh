#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

wait_on() {
  tail --pid=$1 -f /dev/null
}

bash "$BS_PATH/scripts/destroy/backup-le.sh" &
backup_le=$(echo $!)
bash "$BS_PATH/scripts/destroy/backup-code.sh" &
backup_code=$(echo $!)
bash "$BS_PATH/scripts/destroy/backup-ext.sh" &
backup_ext=$(echo $!)
bash "$BS_PATH/scripts/destroy/backup-secrets.sh" &
backup_secrets=$(echo $!)
bash "$BS_PATH/scripts/destroy/push-dirty-repos.sh" &
push_dirty_repos=$(echo $!)

wait
echo -e "Successfully backed up and destroyed" | mail -s "$DOMAIN backed up & destroyed" "$GIT_EMAIL"
wait

bash "$BS_PATH/scripts/destroy/destroy-server.sh"
