#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

wait_on() {
  tail --pid=$1 -f /dev/null
}

bash "$BS_PATH/scripts/create/configure-bs.sh" &
bash "$BS_PATH/scripts/create/create-user.sh" &
create_user=$(echo $!)

bash "$BS_PATH/scripts/create/apt-install-curl-jq.sh"
apt_install_curl_jq=$(echo $!)
{
  wait_on $apt_install_curl_jq
  bash "$BS_PATH/scripts/create/apt-install-node.sh"
} &
apt_install_node=$(echo $!)
{
  wait_on $apt_install_node
  bash "$BS_PATH/scripts/create/apt-install-certbot.sh"
} &
apt_install_certbot=$(echo $!)
{
  wait_on $apt_install_certbot
  bash "$BS_PATH/scripts/create/apt-install-docker.sh"
} &
apt_install_docker=$(echo $!)
{
  wait_on $apt_install_docker
  bash "$BS_PATH/scripts/create/apt-install-fail2ban.sh"
} &
apt_install_fail2ban=$(echo $!)
{
  wait_on $apt_install_fail2ban
  bash "$BS_PATH/scripts/create/apt-install-nginx.sh"
} &
apt_install_nginx=$(echo $!)
{
  wait_on $apt_install_nginx
  bash "$BS_PATH/scripts/create/apt-install-postfix.sh"
} &
apt_install_postfix=$(echo $!)
{
  wait_on $apt_install_postfix
  bash "$BS_PATH/scripts/create/apt-install-time.sh"
} &
apt_install_time=$(echo $!)
{
  wait_on $apt_install_time
  bash "$BS_PATH/scripts/create/apt-install-zsh.sh"
} &
apt_install_zsh=$(echo $!)
{
  wait_on $apt_install_zsh
  bash "$BS_PATH/scripts/create/apt-install-gcloud.sh"
} &
apt_install_gcloud=$(echo $!)
{
  wait_on $apt_install_gcloud
  bash "$BS_PATH/scripts/create/apt-install-yarn-fira-jdk.sh"
} &
apt_install_yarn_fira_jdk=$(echo $!)

{
  wait_on $apt_install_curl_jq
  bash "$BS_PATH/scripts/create/configure-dns.sh"
} &
configure_dns=$(echo $!)
{
  wait_on $apt_install_curl_jq
  wait_on $create_user
  bash "$BS_PATH/scripts/create/install-code.sh"
} &
install_code=$(echo $!)
{
  wait_on $apt_install_node
  wait_on $create_user
  bash "$BS_PATH/scripts/create/clone-repositories.sh"
} &
clone_repositories=$(echo $!)
{
  wait_on $apt_install_certbot
  wait_on $configure_dns
  bash "$BS_PATH/scripts/create/configure-certbot.sh"
} &
configure_certbot=$(echo $!)
{
  wait_on $apt_install_docker
  wait_on $create_user
  bash "$BS_PATH/scripts/create/configure-docker.sh"
} &
configure_docker=$(echo $!)
{
  wait_on $apt_install_fail2ban
  bash "$BS_PATH/scripts/create/configure-fail2ban.sh"
} &
configure_fail2ban=$(echo $!)
{
  wait_on $apt_install_nginx
  bash "$BS_PATH/scripts/create/configure-nginx.sh"
} &
configure_nginx=$(echo $!)
{
  wait_on $apt_install_postfix
  bash "$BS_PATH/scripts/create/configure-postfix.sh"
} &
configure_postfix=$(echo $!)
{
  wait_on $apt_install_time
  bash "$BS_PATH/scripts/create/configure-time.sh"
} &
configure_time=$(echo $!)
{
  wait_on $apt_install_zsh
  wait_on $create_user
  bash "$BS_PATH/scripts/create/configure-zsh.sh"
} &
configure_zsh=$(echo $!)
{
  wait_on $apt_install_gcloud
  wait_on $clone_repositories
  bash "$BS_PATH/scripts/create/configure-gcloud.sh"
} &
configure_gcloud=$(echo $!)

wait
bash "$BS_PATH/scripts/create/start-code-nginx.sh"

echo -e "hostname: $DOMAIN\nuser: $USERNAME\n\nEnsure you run '_ bs -p' and enter a secure password when prompted.\n\n run '_ bs -d' to destroy." | mail -s "$DOMAIN configuration complete" "$GIT_EMAIL"
wait
reboot
