#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Configure DNS'
public_ip=$(curl -fsSL http://icanhazip.com)
mapfile -t existing_domains < <(curl -fsSL -X GET -H "Authorization: Bearer $DO_AUTH_TOKEN" -H "Content-Type: application/json" "https://api.digitalocean.com/v2/domains" | jq -r '.domains[].name')
case "${existing_domains[@]}" in
*$DOMAIN*) exists=true ;;
esac
if [ -z ${exists:-} ]; then
  curl -fsSL -X POST -H "Authorization: Bearer $DO_AUTH_TOKEN" -H "Content-Type: application/json" -d "{\"name\":\"$DOMAIN\", \"ttl\": 600}" "https://api.digitalocean.com/v2/domains"
fi

domain_records=$(curl -fsSL -X GET -H "Authorization: Bearer $DO_AUTH_TOKEN" -H "Content-Type: application/json" "https://api.digitalocean.com/v2/domains/$DOMAIN/records" | jq -c '.domain_records')
host_record=$(echo "$domain_records" | jq -c '.[] | select(.type=="A" and .name=="@")')
wildcard_record=$(echo "$domain_records" | jq -c '.[] | select(.type=="A" and .name=="*")')
acme_record=$(echo "$domain_records" | jq -c '.[] | select(.type=="TXT" and .name=="_acme-challenge")')
spf_record=$(echo "$domain_records" | jq -c '.[] | select(.type=="TXT" and .name=="@")')
host_id=$(echo "$host_record" | jq -r .id)
host_data=$(echo "$host_record" | jq -r .data)
wildcard_id=$(echo "$wildcard_record" | jq -r .id)
wildcard_data=$(echo "$wildcard_record" | jq -r .data)
acme_id=$(echo "$acme_record" | jq -r .id)
spf_id=$(echo "$spf_record" | jq -r .id)

if [ -z "$spf_id" ]; then
  curl -fsSL -X POST -H "Authorization: Bearer $DO_AUTH_TOKEN" -H "Content-Type: application/json" -d "{\"type\":\"TXT\",\"name\":\"@\",\"data\":\"v=spf1 a ~all\", \"ttl\": 600}" "https://api.digitalocean.com/v2/domains/$DOMAIN/records"
fi

if [ ! -z "$acme_id" ]; then
  curl -fsSL -X DELETE -H "Authorization: Bearer $DO_AUTH_TOKEN" -H "Content-Type: application/json" "https://api.digitalocean.com/v2/domains/$DOMAIN/records/$acme_id"
fi

if [ "$host_data" != "$public_ip" ]; then
  if [ -z "$host_id" ]; then
    curl -fsSL -X POST -H "Authorization: Bearer $DO_AUTH_TOKEN" -H "Content-Type: application/json" -d "{\"type\":\"A\",\"name\":\"@\",\"data\":\"$public_ip\", \"ttl\": 600}" "https://api.digitalocean.com/v2/domains/$DOMAIN/records"
  else
    curl -fsSL -X PUT -H "Authorization: Bearer $DO_AUTH_TOKEN" -H "Content-Type: application/json" -d "{\"data\":\"$public_ip\", \"ttl\": 600}" "https://api.digitalocean.com/v2/domains/$DOMAIN/records/$host_id"
  fi
fi

if [ "$wildcard_data" != "$public_ip" ]; then
  if [ -z "$wildcard_id" ]; then
    curl -fsSL -X POST -H "Authorization: Bearer $DO_AUTH_TOKEN" -H "Content-Type: application/json" -d "{\"type\":\"A\",\"name\":\"*\",\"data\":\"$public_ip\", \"ttl\": 600}" "https://api.digitalocean.com/v2/domains/$DOMAIN/records"
  else
    curl -fsSL -X PUT -H "Authorization: Bearer $DO_AUTH_TOKEN" -H "Content-Type: application/json" -d "{\"data\":\"$public_ip\", \"ttl\": 600}" "https://api.digitalocean.com/v2/domains/$DOMAIN/records/$wildcard_id"
  fi
fi
lg 'Configure DNS completed'
