#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Install certbot'
export DEBIAN_FRONTEND=noninteractive

add-apt-repository universe
add-apt-repository -y ppa:certbot/certbot

apt update
apt -y install certbot python-certbot-nginx python3-certbot-dns-digitalocean
lg 'install certbot completed'