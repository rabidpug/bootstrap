#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Configure zsh'
home_directory="$(eval echo ~$USERNAME)"

usermod -s "$(command -v zsh)" "$USERNAME"

find "$BS_PATH/dotfiles" -type f | while read dotfile; do
  ln -sf "$dotfile" "$home_directory"
done

su "$USERNAME" -c "git clone -q https://github.com/junegunn/fzf.git $home_directory/.fzf && $home_directory/.fzf/install --all"
su "$USERNAME" -c "git clone -q https://github.com/zsh-users/antigen.git $home_directory/antigen"


su "$USERNAME" -s $(command -v zsh) -c "source $home_directory/.zshrc"

lg 'Configure zsh completed'
