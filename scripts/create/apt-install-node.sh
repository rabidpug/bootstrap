#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Install node'
export DEBIAN_FRONTEND=noninteractive

curl -fsSL https://deb.nodesource.com/setup_13.x | sudo -E bash -

apt update
apt -y install nodejs build-essential
lg 'Install node completed'
