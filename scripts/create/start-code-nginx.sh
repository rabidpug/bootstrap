#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Start code/nginx'
systemctl start code
systemctl enable code

nginx -s reload
lg 'Start code/nginx completed'
