#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

home_directory="$(eval echo ~$USERNAME)"

lg 'Clone repositories'
su "$USERNAME" -c "git config --global user.email $GIT_EMAIL"
su "$USERNAME" -c "git config --global user.name $GIT_NAME"

if [ ! -z "${REPOS:-}" ]; then
  for repo in "${REPOS[@]}"; do
    repo_name=$repo
    if grep -q git@ <<<"$repo"; then
      su "$USERNAME" -c "git -C $home_directory/projects clone $repo"
      repo_name=$(echo "$repo" | sed -e 's/^.*\/\(.*\).git$/\1/')
    else
      su "$USERNAME" -c "git -C $home_directory/projects clone git@gitlab.com:rabidpug/$repo.git"
    fi
    if [ -f "$home_directory/projects/$repo_name/package.json" ]; then
      su "$USERNAME" -c "npm install -C $home_directory/projects/$repo_name --prefix $home_directory/projects/$repo_name" &
    fi
  done
fi

release_id=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" https://api.github.com/repos/rabidpug/artifacts/releases/latest | jq .id)
assets=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" "https://api.github.com/repos/rabidpug/artifacts/releases/$release_id/assets" | jq -c .)
secrets_id=$(echo "$assets" | jq -rc 'map(select(.label == "garden")) | .[] | .id')

if [ ! -z "${secrets_id:-}" ]; then
  curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" -H "Accept: application/octet-stream" "https://api.github.com/repos/rabidpug/artifacts/releases/assets/$secrets_id" | tar --same-owner -xzp -C "$home_directory"
  chown --recursive "$USERNAME:$USERNAME" "$home_directory/.secrets"
find "$home_directory/.secrets/"* -maxdepth 0 -type d | while read path; do
  repo=$(basename $path)
  if [ -d "$home_directory/projects/$repo" ]; then
    lg "Linking secrets for $repo"
    su "$USERNAME" -c "cp -rlT $path $home_directory/projects/$repo" || :
  fi
done

fi

lg 'Clone repositories completed'
