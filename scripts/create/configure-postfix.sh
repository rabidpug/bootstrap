#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Configure postfix'
sed -i 's/^inet_interfaces = .*$/inet_interfaces = loopback-only/g' /etc/postfix/main.cf
sed -i 's/^mydestination = .*$/mydestination = $myhostname, localhost.$mydomain, $mydomain/g' /etc/postfix/main.cf
systemctl restart postfix
echo "root: $GIT_EMAIL" >>/etc/aliases
newaliases
lg 'Configure postfix completed'
