#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Install code'
home_directory="$(eval echo ~$USERNAME)"
if [ -z "${CODE_PRE:-}" ]; then
  release=$(curl -fsSL https://api.github.com/repos/cdr/code-server/releases/latest | jq -c .)
else 
  release=$(curl -fsSL https://api.github.com/repos/cdr/code-server/releases | jq -c '. | .[0]')
fi

new=0;
if [ -z "$(echo -E "$release" | jq -r .name | grep '^2.')" ]; then
  lg 'Code server v3'
  new=1;
  pre="code-server-"
else
  lg 'Code server v2'
  pre="code-server"
fi
name="$pre$(echo -E "$release" | jq -r .name)-$(uname -s | tr '[:upper:]' '[:lower:]')-$(uname --m)"
asset=$(echo -E "$release" | jq ".assets[] | select(.name==\"$name.tar.gz\") | .id")
curl -fsSL -H "Accept: application/octet-stream" "https://api.github.com/repos/cdr/code-server/releases/assets/$asset" | tar -zx -C /tmp
if [ "$new" = "1" ]; then
  mv /tmp/$name /usr/local/code-server
  chmod +x /usr/local/code-server/code-server
  ln -sf /usr/local/code-server/code-server /usr/local/bin
else
  mv "/tmp/$name/code-server" /usr/local/bin
  rm -rf "/tmp/$name"
  chmod +x /usr/local/bin/code-server
fi

mkdir -p /.secrets
echo "PASSWORD=${PASSWORD}" >> /.secrets/.code-pass

cp "$BS_PATH/configs/code.service" /etc/systemd/system
sed -i "s+%%USER%%+$USERNAME+g" /etc/systemd/system/code.service
sed -i "s+%%HOME%%+$home_directory+g" /etc/systemd/system/code.service

release_id=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" https://api.github.com/repos/rabidpug/artifacts/releases/latest | jq .id)
assets=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" "https://api.github.com/repos/rabidpug/artifacts/releases/$release_id/assets" | jq -c .)
asset_id=$(echo "$assets" | jq -rc 'map(select(.label == "backup")) | .[] | .id')
ext_id=$(echo "$assets" | jq -rc 'map(select(.label == "ext")) | .[] | .id')

if [ ! -z "${asset_id:-}" ]; then
  curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" -H "Accept: application/octet-stream" "https://api.github.com/repos/rabidpug/artifacts/releases/assets/$asset_id" | tar --same-owner -xzp -C "$home_directory"
fi

if [ ! -z "${ext_id:-}" ]; then
  curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" -H "Accept: application/octet-stream" "https://api.github.com/repos/rabidpug/artifacts/releases/assets/$ext_id" | while read ext; do
    vsix=$(ls -d $home_directory/.local/share/code-server/vsix/* | grep $ext || :)
    {
      res=$(su "$USERNAME" -c "/usr/local/bin/code-server --install-extension $ext --force" &>/dev/null || echo fail)
      if [ ! -z "$vsix" ]; then
        if [ "$res" = "fail" ]; then
          su "$USERNAME" -c "/usr/local/bin/code-server --install-extension $vsix --force"
        else
          rm -f "$vsix"
        fi;
      elif [ "$res" = "fail" ]; then
        echo "$ext" >> "$home_directory/.local/share/code-server/vsix/missing.txt"
      fi
    } &
  done
fi
wait
lg 'Install code completed'
