#!/bin/bash
set -eou pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Configure gcloud'
home_directory="$(eval echo ~$USERNAME)"
curl -fsSL https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -o /usr/local/bin/cloud_sql_proxy
chmod +x /usr/local/bin/cloud_sql_proxy
ls -d "$home_directory/.secrets/gcloud/"* | while read keyfile; do
  su "$USERNAME" -c "gcloud auth activate-service-account --key-file=$keyfile"
done

lg 'Configure gcloud completed'
