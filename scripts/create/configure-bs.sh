#!/bin/bash
set -eou pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Configure bs'
cat <<EOT >/etc/logrotate.d/bs
/var/log/bs.log {
daily
copytruncate
missingok
dateext
rotate 7
nocompress
}

EOT

chmod +x "$BS_PATH/bs"
ln -sf "$BS_PATH/bs" /usr/local/bin

find "$BS_PATH/cron/" -mindepth 1 -type d | while read folder; do
  interval=$(basename "$folder")
  find "$folder" -type f | while read job; do
    chmod +x "$job"
    ln -sf "$job" "/etc/cron.$interval"
  done
done

sed -i 's/ 6\t/ 4\t/g' /etc/crontab
lg 'Configure bs completed'
