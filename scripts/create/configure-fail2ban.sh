#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Configure fail2ban'
cp "$BS_PATH/configs/jail.local" /etc/fail2ban
sed -i "s/%%EMAIL%%/$GIT_EMAIL/g" /etc/fail2ban/jail.local
cp "$BS_PATH/configs/nginx-http-auth.conf" /etc/fail2ban/filter.d
cp "$BS_PATH/configs/nginx-noscript.conf" /etc/fail2ban/filter.d
cp /etc/fail2ban/filter.d/apache-badbots.conf /etc/fail2ban/filter.d/nginx-badbots.conf

systemctl restart fail2ban
lg 'Configure fail2ban completed'
