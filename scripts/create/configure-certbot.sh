#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Configure certbot'
mkdir -p /.secrets
echo "dns_digitalocean_token = $DO_AUTH_TOKEN" >/.secrets/digitalocean.ini
chmod 600 /.secrets/digitalocean.ini

release_id=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" https://api.github.com/repos/rabidpug/artifacts/releases/latest | jq .id)
assets=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" "https://api.github.com/repos/rabidpug/artifacts/releases/$release_id/assets" | jq -c .)
le_id=$(echo "$assets" | jq -rc 'map(select(.label == "le")) | .[] | .id')

if [ ! -z "${le_id:-}" ]; then
  curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" -H "Accept: application/octet-stream" "https://api.github.com/repos/rabidpug/artifacts/releases/assets/$le_id" | tar --same-owner -xzp -C "/etc/letsencrypt" || :
fi

if [ ! -d "/etc/letsencrypt/live/$DOMAIN" ] || [ -z "$(find "etc/letsencrypt/live/$DOMAIN" -mtime -10)" ]; then
  certbot certonly --dns-digitalocean --dns-digitalocean-credentials /.secrets/digitalocean.ini --dns-digitalocean-propagation-seconds 70 -n --agree-tos -m $GIT_EMAIL -d $DOMAIN -d "*.$DOMAIN" --expand
fi
lg 'Configure certbot completed'
