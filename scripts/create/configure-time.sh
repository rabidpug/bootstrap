#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Configure time'
ln -fs "/usr/share/zoneinfo/$TZ" /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata
lg 'Configure time completed'
