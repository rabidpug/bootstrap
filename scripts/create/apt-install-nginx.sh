#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Install nginx'
export DEBIAN_FRONTEND=noninteractive

apt -y install nginx
lg 'Install nginx completed'
