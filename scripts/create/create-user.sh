#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Create user'
useradd --create-home --shell "/bin/bash" --groups sudo "$USERNAME"

encrypted_root_pw="$(grep root /etc/shadow | cut --delimiter=: --fields=2)"
if [ "$encrypted_root_pw" != "*" ]; then
  echo "$USERNAME:$encrypted_root_pw" | chpasswd --encrypted
  passwd --lock root
else
  passwd --delete "$USERNAME"
fi
chage --lastday 0 "$USERNAME"
echo "$USERNAME ALL=(ALL:ALL) NOPASSWD: ALL" | tee -a "/etc/sudoers.d/$USERNAME"

home_directory="$(eval echo ~$USERNAME)"
mkdir -p "$home_directory/.ssh"
mkdir -p "$home_directory/projects"

cp /root/.ssh/authorized_keys "$home_directory/.ssh"
echo -e "$ID_RSA" >"$home_directory/.ssh/id_rsa"
echo "$ID_RSA_PUB" >"$home_directory/.ssh/id_rsa.pub"
ssh-keyscan -H gitlab.com >>"$home_directory/.ssh/known_hosts"
ssh-keyscan -H github.com >>"$home_directory/.ssh/known_hosts"
chmod 0700 "$home_directory/.ssh"
chmod 0600 "$home_directory/.ssh/authorized_keys"
chmod 0600 "$home_directory/.ssh/id_rsa"
chmod 0600 "$home_directory/.ssh/id_rsa.pub"
chown --recursive "$USERNAME":"$USERNAME" "$home_directory"
sed -i 's/^PermitRootLogin.*/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config
sed -i 's/^PasswordAuthentication.*/PasswordAuthentication no/g' /etc/ssh/sshd_config
if sshd -t -q; then
  systemctl restart sshd
fi

fallocate -l 4G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile swap swap defaults 0 0' | tee -a /etc/fstab
echo vm.swappiness=10 | tee -a /etc/sysctl.conf

echo fs.inotify.max_user_watches=524288 | tee -a /etc/sysctl.conf
sysctl -p
lg 'Create user completed'
