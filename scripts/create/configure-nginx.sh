#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

cp "$BS_PATH/configs/ssl.conf" /etc/nginx/conf.d
ufw allow OpenSSH
ufw allow 'Nginx Full'
ufw --force enable

rm /etc/nginx/sites-enabled/default
cp "$BS_PATH/configs/nginx-site" "/etc/nginx/sites-enabled/$DOMAIN"
sed -i "s/%%DOMAIN%%/$DOMAIN/g" "/etc/nginx/sites-enabled/$DOMAIN"
sed -i "s/%%DOMAINESC%%/${DOMAIN/./\\.}/g" "/etc/nginx/sites-enabled/$DOMAIN"
lg 'Configure nginx completed'
