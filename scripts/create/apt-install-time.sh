#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Install time'
export DEBIAN_FRONTEND=noninteractive

apt -y install tzdata ntp
lg 'Install time completed'
