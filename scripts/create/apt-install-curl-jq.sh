#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Install curl/jq'
export DEBIAN_FRONTEND=noninteractive

apt update
apt -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common jq
lg 'Install curl/jq completed'