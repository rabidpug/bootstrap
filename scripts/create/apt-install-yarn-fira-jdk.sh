#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Upgrade and Install yarn/fira/jdk'
export DEBIAN_FRONTEND=noninteractive

curl -fsSL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
add-apt-repository "deb https://dl.yarnpkg.com/debian/ stable main"
curl -fsSL https://deb.nodesource.com/setup_13.x | sudo -E bash -

apt update
apt -y upgrade
apt -y dist-upgrade
apt -y install yarn fonts-firacode default-jdk
lg 'Upgrade and Install yarn/fira/jdk completed'
