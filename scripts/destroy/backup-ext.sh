#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Backup ext'

home_directory="$(eval echo ~$USERNAME)"

release_id=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" https://api.github.com/repos/rabidpug/artifacts/releases/latest | jq .id)
assets=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" "https://api.github.com/repos/rabidpug/artifacts/releases/$release_id/assets" | jq -c .)
ext_id=$(echo "$assets" | jq -rc 'map(select(.label == "ext")) | .[] | .id')

code-server --list-extensions >/tmp/ext.txt

if [ ! -z "${ext_id:-}" ]; then
  curl -fsSL -X DELETE -H "Authorization: token $GITHUB_AUTH_TOKEN" "https://api.github.com/repos/rabidpug/artifacts/releases/assets/$ext_id" || :
fi

curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" -H "Content-Type: application/octet-stream" --data-binary "@/tmp/ext.txt" "https://uploads.github.com/repos/rabidpug/artifacts/releases/$release_id/assets?name=ext.txt&label=ext"

rm -f "/tmp/ext.txt"
lg 'Backup ext completed'
