#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Backup secrets'
home_directory="$(eval echo ~$USERNAME)"

release_id=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" https://api.github.com/repos/rabidpug/artifacts/releases/latest | jq .id)
assets=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" "https://api.github.com/repos/rabidpug/artifacts/releases/$release_id/assets" | jq -c .)
asset_id=$(echo "$assets" | jq -rc 'map(select(.label == "garden")) | .[] | .id')

tar -cpzf "/tmp/garden.tar.gz" -C "$home_directory" .secrets

if [ ! -z "${asset_id:-}" ]; then
  curl -fsSL -X DELETE -H "Authorization: token $GITHUB_AUTH_TOKEN" "https://api.github.com/repos/rabidpug/artifacts/releases/assets/$asset_id" || :
fi

curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" -H "Content-Type: application/octet-stream" --data-binary "@/tmp/garden.tar.gz" "https://uploads.github.com/repos/rabidpug/artifacts/releases/$release_id/assets?name=garden.tar.gz&label=garden"

rm -f "/tmp/garden.tar.gz"
lg 'Backup secrets completed'
