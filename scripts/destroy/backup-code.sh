#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Backup code'
home_directory="$(eval echo ~$USERNAME)"

release_id=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" https://api.github.com/repos/rabidpug/artifacts/releases/latest | jq .id)
assets=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" "https://api.github.com/repos/rabidpug/artifacts/releases/$release_id/assets" | jq -c .)
asset_id=$(echo "$assets" | jq -rc 'map(select(.label == "backup")) | .[] | .id')

tar --exclude=".local/share/code-server/extensions" --exclude=".local/share/code-server/logs" -cpzf "/tmp/backup.tar.gz" -C "$home_directory" .local

if [ ! -z "${asset_id:-}" ]; then
  curl -fsSL -X DELETE -H "Authorization: token $GITHUB_AUTH_TOKEN" "https://api.github.com/repos/rabidpug/artifacts/releases/assets/$asset_id" || :
fi

curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" -H "Content-Type: application/octet-stream" --data-binary "@/tmp/backup.tar.gz" "https://uploads.github.com/repos/rabidpug/artifacts/releases/$release_id/assets?name=backup.tar.gz&label=backup"

rm -f "/tmp/backup.tar.gz"
lg 'Backup code completed'
