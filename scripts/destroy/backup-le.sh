#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Backup le'
home_directory="$(eval echo ~$USERNAME)"

release_id=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" https://api.github.com/repos/rabidpug/artifacts/releases/latest | jq .id)
assets=$(curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" "https://api.github.com/repos/rabidpug/artifacts/releases/$release_id/assets" | jq -c .)
le_id=$(echo "$assets" | jq -rc 'map(select(.label == "le")) | .[] | .id')

tar -cpzf "/tmp/le.tar.gz" -C "/etc/letsencrypt" .

if [ ! -z "${le_id:-}" ]; then
  curl -fsSL -X DELETE -H "Authorization: token $GITHUB_AUTH_TOKEN" "https://api.github.com/repos/rabidpug/artifacts/releases/assets/$le_id" || :
fi

curl -fsSL -H "Authorization: token $GITHUB_AUTH_TOKEN" -H "Content-Type: application/octet-stream" --data-binary "@/tmp/le.tar.gz" "https://uploads.github.com/repos/rabidpug/artifacts/releases/$release_id/assets?name=le.tar.gz&label=le"

rm -f "/tmp/le.tar.gz"
lg 'Backup le completed'
