#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

lg 'Push dirty repos'
home_directory="$(eval echo ~$USERNAME)"

find "$home_directory/projects" -mindepth 1 -maxdepth 1 -type d | while read repo; do
  if [ -d "$repo" ] && [ ! -z "$(git -C "$repo" status -s)" ] && [ ! -z "$(git -C "$repo" remote)" ]; then
    lg "$(basename $repo) is dirty"
    su "$USERNAME" -c "git -C $repo add ."
    su "$USERNAME" -c "git -C $repo commit -m 'Updated'"
    su "$USERNAME" -c "git -C $repo push"
  fi
done
lg 'Push dirty repos completed'