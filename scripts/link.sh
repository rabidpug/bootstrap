#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

home_directory="$(eval echo ~$USERNAME)"

find "$home_directory/.secrets/"* -maxdepth 0 -type d | while read path; do
  repo=$(basename $path)
  if [ -d "$home_directory/projects/$repo" ]; then
    lg "Linking secrets for $repo"
    cp -rlT "$path" "$home_directory/projects/$repo" || :
  fi
done
