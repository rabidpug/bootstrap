BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"

lg() {
  echo ">> [$(date '+%Y-%m-%d %H:%M:%S')]: $@" >&3
}
