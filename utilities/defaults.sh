if [ "${USERNAME:-}" = "root" ]; then
  unset USERNAME
fi

USERNAME="${USERNAME:-m}"
PASSWORD="${PASSWORD:-pass}"
GIT_NAME="${GIT_NAME:-'Matt Cuneo'}"
GIT_EMAIL="${GIT_EMAIL:-m@jcuneo.com}"
DOMAIN="${DOMAIN:-$(hostname -f)}"
TZ="${TZ:-Australia/Sydney}"
