#!/bin/bash
set -euo pipefail

BS_PATH=/usr/local/bootstrap

source "$BS_PATH/.env"
source "$BS_PATH/utilities/defaults.sh"
source "$BS_PATH/utilities/lg.sh"

if [ -z "${DEBUG:-}" ]; then
  exec 3>&1 &>/dev/null
else
  exec 3>&1
fi

while getopts "cldps:k:" opt; do
  case $opt in
  d)
    other=true
    lg 'enter Y to destroy'
    read res
    if [ "$res" = "Y" ]; then
      bash "$BS_PATH/scripts/destroy.sh"
    fi
    ;;
  p)
    other=true
    lg 'Enter a new password'
    read -s pass
    lg 'Confirm new password'
    read -s passc
    if [ "$pass" != "$passc" ]; then
      lg "Passwords don't match!"
    else
      echo "PASSWORD=${pass}" >/.secrets/.code-pass
      service code restart
    fi
    ;;
  l)
    other=true
    bash "$BS_PATH/scripts/link.sh"
    ;;
  s)
    other=true
    bash "$BS_PATH/scripts/${OPTARG}.sh"
    ;;
  c)
    other=true
    bash "$BS_PATH/scripts/clean.sh"
    ;;
  k)
    other=true
    ports=("$OPTARG")
    until [[ $(eval "echo \${$OPTIND}") =~ ^-.* ]] || [ -z $(eval "echo \${$OPTIND}") ]; do
      ports+=($(eval "echo \${$OPTIND}"))
      OPTIND=$((OPTIND + 1))
    done
    for port in "${ports[@]}"; do
      kill -9 $(lsof -t -i:$port)
    done
    ;;

  esac
done

if [ -z "${other:-}" ]; then
  bash "$BS_PATH/scripts/create.sh"
fi
